<?php

declare(strict_types=1);


namespace App\Component\News;


use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;

class NewManager
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function save(News $news, bool $isNeedFlush = false): void
    {
        $this->entityManager->persist($news);
        if ($isNeedFlush) {
            $this->entityManager->flush();
        }
    }
}