<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\NewCreateAction;
use App\Repository\NewsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'createNew' => [
            'method' => 'post',
            'path' => 'news/my',
            'controller' =>NewCreateAction::class,
        ]
    ],
    itemOperations: ['get', 'delete'],
    denormalizationContext: ['groups' => ['news:write']],
    normalizationContext: ['groups' => ['news:read']]
)]
class News
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(['news:read'])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['news:read'])]
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['news:read', 'news:write'])]
    private $link;

    /**
     * @ORM\Column(type="text")
     */
    #[Groups(['news:read', 'news:write'])]
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    #[Groups(['news:read', 'news:write'])]
    private $pubDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['news:read', 'news:write'])]
    private $author;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['news:read', 'news:write'])]
    private $img;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPubDate(): ?\DateTimeInterface
    {
        return $this->pubDate;
    }

    public function setPubDate(\DateTimeInterface $pubDate): self
    {
        $this->pubDate = $pubDate;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }
}
