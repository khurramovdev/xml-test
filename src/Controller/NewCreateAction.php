<?php

declare(strict_types=1);


namespace App\Controller;


use App\Component\News\NewFactory;
use App\Component\News\NewManager;
use App\Entity\News;

class NewCreateAction
{
    public function __invoke(News $data, NewFactory $newFactory, NewManager $newManager): News
    {
        $news = $newFactory->create(
            $data->getTitle(),
            $data->getLink(),
            $data->getDescription(),
            $data->getPubDate(),
            $data->getAuthor(),
            $data->getImg()
        );

        $newManager->save($news, true);
        return $news;
    }
}