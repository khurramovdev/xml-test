<?php

declare(strict_types=1);


namespace App\Component\News;

use App\Entity\News;

class NewFactory
{
    public function create(
        string $title,
        string $link,
        string $description,
        \DateTimeInterface $pubDate,
        string $author,
        string $img
    ): News {
        $news = new News();
        $news->setTitle($title);
        $news->setLink($link);
        $news->setDescription($description);
        $news->setPubDate(new \DateTime());
        $news->setAuthor($author);
        $news->setImg($img);

        return $news;
    }
}